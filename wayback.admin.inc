<?php 

function wayback_admin_settings() {
  $form = array();
  
  $interval = array(0, 3*3600, 6*3600, 12*3600, 24*3600, 2*24*3600, 3*24*3600, 7*24*3600, 14*24*3600);

  $form['wayback_cron_interval'] = array(
    '#type' => 'select',
    '#options' => drupal_map_assoc($interval, 'format_interval'),
    '#title' => t('Cron interval'),
    '#description' => t("This sets the frequency of checking. An interval of 0 means never."),
    '#default_value' => variable_get('wayback_cron_interval', 86400),
  );
  
  $form['wayback_cache_external_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Store external images'),
    '#default_value' => variable_get('wayback_cache_external_image', FALSE),
    '#description' => t('If this is checked, we will store external image links'),
  );
  
  $form['wayback_directory_path'] = array(
    '#type' => 'textfield',
    '#title' => t('File system path'),
    '#default_value' => wayback_directory_path(),
    '#maxlength' => 255, 
    '#description' => t('A file system path where the wayback files should be stored. This directory must exist and be writable by Drupal.'),
    '#after_build' => array('system_check_directory'),
  );
  
  $form['wayback_path_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Path Settings'),
    '#collapsible' => TRUE,
  );

  $form['wayback_path_settings']['wayback_path'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Path'),
    '#default_value' => variable_get('wayback_path', 'wayback'),
    '#description'   => t('[token] may not be used for the first segment of a path.'),
  );

  $form['wayback_path_settings']['wayback_path_tokens'] = array(
    '#value' => theme('token_help', 'wayback'),
  );

  $form = system_settings_form($form);

  $form['#validate'][] = 'wayback_admin_settings_validate';
  $form['#submit'][]   = 'wayback_admin_settings_submit';

  return $form;
}

function wayback_admin_settings_validate($form, &$form_state) {
  $path = $form_state['values']['wayback_path'];
  $first_segment = arg(0, $path);
  if (preg_match('/\[[^\s\]]*\]/', $first_segment)) {
    form_set_error('wayback_path', $first_segment . ' should not be used for the first segment of a path');
  }
}

function wayback_admin_settings_submit($form, &$form_state) {
  menu_rebuild();
}
