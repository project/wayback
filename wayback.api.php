<?php


/**
 * @file
 * Hooks provided by the Wayback module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * This hook can be used to alter the front page html or headers before its
 * stored in database.
 * 
 * @param string $html
 *   The html code for the front page
 * @param string headers
 *   Front page Header information
 *
 */
function hook_wayback_save(&$html, &$headers) {
}

/**
 * This hook can be used to change the html code before its rendered
 * One can alter additional information at time of display.
 *
 * @param string $html
 *   The html code
 * @param int $timestamp
 *   The timestamp when the file was stored in 
 *
 */
function hook_wayback_view_alter(&$html, $timestamp) {
}

/**
 * @} End of "addtogroup hooks".
 */

