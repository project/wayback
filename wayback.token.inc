<?php

function wayback_token_values($type, $object = NULL, $options = array()) {
  $values = array();
  if ($type == 'wayback') {
    $timestamp = $object;
    $tokens['yyyy']  = format_date($timestamp, 'custom', 'Y');
    $tokens['yy']    = format_date($timestamp, 'custom', 'y');
    $tokens['month'] = format_date($timestamp, 'custom', 'F');
    $tokens['mon']   = format_date($timestamp, 'custom', 'M');
    $tokens['mm']    = format_date($timestamp, 'custom', 'm');
    $tokens['m']     = format_date($timestamp, 'custom', 'n');
    $tokens['ww']    = format_date($timestamp, 'custom', 'W');
    $tokens['day']   = format_date($timestamp, 'custom', 'l');
    $tokens['ddd']   = format_date($timestamp, 'custom', 'D');
    $tokens['dd']    = format_date($timestamp, 'custom', 'd');
    $tokens['d']     = format_date($timestamp, 'custom', 'j');
    $tokens['time']  = format_date($timestamp, 'custom', 'H:i');
  }
  return $tokens;
}

function wayback_token_list($type = 'all') {
  if ($type == 'wayback') {
    $tokens['wayback']['yyyy']   = t("Date year (four digit)");
    $tokens['wayback']['yy']     = t("Date year (two digit)");
    $tokens['wayback']['month']  = t("Date month (full word)");
    $tokens['wayback']['mon']    = t("Date month (abbreviated)");
    $tokens['wayback']['mm']     = t("Date month (two digit, zero padded)");
    $tokens['wayback']['m']      = t("Date month (one or two digit)");
    $tokens['wayback']['ww']     = t("Date week (two digit)");
    $tokens['wayback']['day']    = t("Date day (full word)");
    $tokens['wayback']['ddd']    = t("Date day (abbreviation)");
    $tokens['wayback']['dd']     = t("Date day (two digit, zero-padded)");
    $tokens['wayback']['d']      = t("Date day (one or two digit)");
    $tokens['wayback']['time']   = t("Time H:i");
  }

  return $tokens;
}

