<?php

class waybackDocument extends DOMDocument {
  var $css_files;
  var $js_files;
  var $img_files;
    
  public function __toString() {
    return $this->saveHTML();
  }

  function css_extract() {
    
    $css_files = $this->getElementsByTagName('link');
    foreach($css_files as $css_file) {
      $attribute = $css_file->getAttribute('type');
      if ($attribute == 'text/css') {
        $src = $css_file->getAttribute('href');
        if ($src) {
          _wayback_cache_css($src);
          $css_file->setAttribute('href', url($src, array('absolute' => TRUE)));
        }
      }      
    }
  }
  
  function js_extract() {
    $js_files = $this->getElementsByTagName('script');
    foreach ($js_files as $js_file) {
      $attribute = $js_file->getAttribute('type');
      if ($attribute == 'text/javascript') {
        $src = $js_file->getAttribute('src');
        if ($src) {
          _wayback_cache_js($src);
          $js_file->setAttribute('href', url($src, array('absolute' => TRUE)));
        }
      }
    }
  }
  
  function img_extract() {
    $img_files = $this->getElementsByTagName('img');
    foreach ($img_files as $img_file) {
      $src = $img_file->getAttribute('src');
      if ($src) {
        _wayback_cache_image($src);
        $img_file->setAttribute('src', url($src, array('absolute' => TRUE)));
      }
    }
  }
}


/**
 * Create directory recursively 
 */
function wayback_check_directory_recursively($directory) {
  // Recursively add new directories if the parent does not exist.
  $full_path = '';
  foreach (explode('/', $directory) as $path) {
    $full_path .= $path;
    $return = file_check_directory($full_path, FILE_CREATE_DIRECTORY);
    $full_path .=  '/';
  }
  return $return;
}

/**
 * function to check if the url is an internal URL and return the path relative to drupal directory  
 */
function wayback_is_internal_url(&$url) {
  global $base_root, $base_path;

  $parsed_url = parse_url($url);
  
  // Check if the url is internal if 'host' is not set then its surely internal
  // Else compare the base_root
  if (!isset($parsed_url['host']) || substr($url, 0, strlen($base_root)) == $base_root) {
    $url = $parsed_url['path'];
    $url = substr($url, strlen($base_path));
    return str_replace(file_directory_path(), wayback_directory_path(), $url);
  }
  return FALSE;
}

/**
 * Stores css and update html 
 */
function _wayback_cache_css(&$src) {
  global $base_root, $base_path;
  if ($dest = wayback_is_internal_url($src)) {
    
    $directory = dirname($dest);
    if (wayback_check_directory_recursively($directory)) {
      $dest = file_create_path($dest);
      // Src is passed by reference
      if (!is_file($dest)) {
        file_copy($src, $dest, FILE_EXISTS_ERROR);
      }
      else {
        $src = $dest;
      }
    }
  }
  // @TODO: Add support for downloading external css/js files
}

/**
 * Stores js and update html 
 */
function _wayback_cache_js(&$src) {
  _wayback_cache_css($src);
}

/**
 * Stores images and update html 
 */
function _wayback_cache_image(&$src) {
  if ($dest = wayback_is_internal_url($src)) {
    $directory = dirname($dest);
    if (wayback_check_directory_recursively($directory)) {
      $dest = file_create_path($dest);
      if (!file_exists($dest)) {
        file_copy($src, $dest);
      }
      $src = $dest;
    }
  } 
  elseif (variable_get('wayback_cache_external_image', FALSE)) {
    $src = wayback_download_from_remote($src, 'ext_images');
  }
}

/**
 * Download data from remote site
 */
function wayback_download_from_remote($url, $directory = '') {
  
  // Check the headers to make sure it exists.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, TRUE);
  curl_setopt($ch, CURLOPT_NOBODY, TRUE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  // Causes a warning if PHP safe mode is on.
  @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_exec($ch);
  $info = curl_getinfo($ch);
  curl_close($ch);
  
  if ($info['http_code'] != 200) {
    // do not attempt to download broken links
    return;
  }
  
  // In case of redirect
  $url = $info['url'];
  $url_info = parse_url($url);
  $pathinfo = pathinfo($url_info['path']);
  $filename = rawurldecode(basename($url_info['path']));
  $filename = wayback_clean_filename($filename);
  $filepath = file_create_filename($filename, file_directory_temp());
  
  if (empty($pathinfo['extension'])) {
    return;
  }
  
  $result = drupal_http_request($url);
  
  $info = array();
  $info['filemime']  = $result->headers['Content-Type'];
  $info['filesize']  = $result->headers['Content-Length'];
  $info['data']      = $result->data;
  list($dummy, $ext) = explode('/', $info['filemime']);
  
  $path = wayback_directory_path() . '/' . $directory;
  file_check_directory($path, FILE_CREATE_DIRECTORY);
  $path = file_create_path($path);
  $info['filepath']  = $path . '/' . $file_name;
  $info['filename']  = $file_name;
  
  
  $exists = is_file($info['filepath']);
  if (!$exists) {
    file_save_data($info['data'], $info['filepath'], FILE_EXISTS_REPLACE);
  }
  else {
    // @TODO:
    // If file already exists, check for properties of file
    // If file has same properties eg. mimetype, filesize
    // Then don't rename else rename the file
  }
  
  return $info['filepath'];
}


/**
 * Clean up the file name, and transliterating.
 *
 * @param $filepath
 *   A string containing a file name or full path. Only the file name will
 *   actually be modified.
 * @return
 *   A file path with a cleaned-up file name.
 */
function wayback_clean_filename($filepath) {
  $filename = basename($filepath);

  if (module_exists('transliteration')) {
    module_load_include('inc', 'transliteration');

    $langcode = NULL;
    if (!empty($_POST['language'])) {
      $languages = language_list();
      $langcode = isset($languages[$_POST['language']]) ? $_POST['language'] : NULL;
    }
    $filename = transliteration_clean_filename($filename, $langcode);
  }

  // Because this transfer mechanism does not use file_save_upload(), we need
  // to manually munge the filename to prevent dangerous extensions.
  // See file_save_upload().
  $extensions = 'jpg jpeg gif png css js txt html doc xls pdf ppt pps odt ods odp';
  
  $filename = file_munge_filename($filename, $extensions);

  $directory = dirname($filepath);
  return ($directory ? $directory . '/' : $directory) . $filename;
} 
